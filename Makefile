NAME := apish

.DEFAULT_GOAL := help
.PHONY: build clean docs help publish test report

help: ## Show this help
	@echo "${NAME}"
	@echo
	@fgrep -h "##" $(MAKEFILE_LIST) | \
	fgrep -v fgrep | sed -e 's/## */##/' | column -t -s##

##

all: ## Generate all artifacts
all: build docs

build: ## Build the package
build: .check-version
	python setup.py sdist

clean: ## Remove generated artifacts
	@rm -rf build dist apian.egg-info docs/build
	@rm -f VERSION

docs: ## Build the documentation
	make -C docs html

init: ## Set up the environment
	@for req in requirements/*.txt; do \
	   pip install -r $$req; \
	done

publish: ## Publish the package to PyPI
	@twine upload dist/* --repository-url https://upload.pypi.org/legacy/ \
						 -u ${PYPI_USER} -p ${PYPI_PASSWORD} \
						 --skip-existing

qa: qa/mypy qa/test qa/pylint

qa/mypy:
	@mypy src/apish test/test_apish

qa/pylint:
	@pylint src/apish test/test_apish

qa/test: ## Test the package
	@coverage run --source src -m pytest
	@coverage report -m

version: ## Generate the VERSION file
	@git describe --tag --always | tee VERSION

##

.check-version:
	@if [ ! -f VERSION ]; then \
		echo "Run 'make version' to create a VERSION file"; \
		exit 1; \
	fi;
