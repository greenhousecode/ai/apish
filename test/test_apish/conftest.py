import io
import logging

import pytest
from fastapi.testclient import TestClient

from apish import Application, Contact, Metadata, Version


@pytest.fixture
def stream():
    return io.StringIO()


@pytest.fixture
def handler(stream):
    handler = logging.StreamHandler(stream)
    handler.formatter = logging.Formatter()
    yield handler


@pytest.fixture
def log_stream(handler, stream):
    root = logging.getLogger()
    root.handlers = [handler]
    root.setLevel(logging.INFO)
    return stream


@pytest.fixture
def metadata():
    return Metadata(
        title="<title>",
        version=Version(app="v0.1.1", api="v0.1.0"),
        description=None,
        contact=Contact(name="name", url="http://test.com", email=None),
        api_id="49786b4b-1889-46ec-bd72-27f332436e6f",
        audience="company-internal",
    )


@pytest.fixture
def app(metadata):
    return Application("", metadata)


def client(app):
    return TestClient(app)
