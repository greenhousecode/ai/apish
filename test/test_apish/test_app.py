import json
import re
from typing import Any, List, Optional

import pytest
from fastapi.testclient import TestClient
from pydantic import BaseModel
from starlette.exceptions import HTTPException

from apish import CustomRoute, Namespace, Problem, Resource

ns = Namespace([])


def _parse_logs(stream) -> List[str]:
    stream.flush()
    stream.seek(0)
    return stream.read().strip().split("\n")


def _extract_json_body(logs: List[str]) -> Optional[Any]:
    def extract(line):
        m = re.match(r".*body: (?:b')?(.*)'\)", line)
        if m is None:
            return None
        try:
            return json.loads(m.group(1))
        except json.JSONDecodeError:
            return None

    for line in logs:
        body = extract(line)
        if body is not None:
            return body
    return None


class Foo(BaseModel):
    bar: int


class Baz(BaseModel):
    quux: str


@ns.route("/foo")
class FooResource(Resource):
    async def post(self, _body: Foo) -> Baz:
        return Baz(quux="quux")

    async def get(self):
        raise HTTPException(400, "error!")


@ns.route("/error-async")
class ErrorAsyncResource(Resource):
    async def post(self):
        raise Exception("error!")


@ns.route("/error-sync")
class ErrorSyncResource(Resource):
    def post(self):
        raise Exception("error!")


@ns.route("/exception-async")
class ExceptionAsyncResource(Resource):
    async def post(self):
        raise HTTPException(400)


@ns.route("/exception-sync")
class ExceptionSyncResource(Resource):
    def post(self):
        raise HTTPException(400)


class TestExceptionHandlers:
    def test_validation_error_returns_problem(self, app):
        app.add(ns)
        client = TestClient(app)
        resp = client.post("/foo", json={"bar": "bla"})
        obj = resp.json()

        try:
            Problem(**obj)
        except:  # pylint: disable=bare-except
            assert False, obj

    def test_http_exception_returns_problem(self, app):
        app.add(ns)
        client = TestClient(app)
        resp = client.get("/foo")
        obj = resp.json()

        try:
            Problem(**obj)
        except:  # pylint: disable=bare-except
            assert False, obj

    @pytest.mark.parametrize("path", ["error-async", "error-sync"])
    def test_log_request_body_on_unhandled_exception(self, app, log_stream, path):
        app.add(ns)
        client = TestClient(app)
        body = {"foo": "bar"}
        with pytest.raises(Exception):
            client.post(path, json=body)
        logs = _parse_logs(log_stream)
        actual = _extract_json_body(logs)

        assert actual == body, "Body not logged"

    @pytest.mark.parametrize("path", ["exception-async", "exception-sync"])
    def test_do_not_log_body_on_handled_exception(self, app, log_stream, path):
        app.add(ns)
        client = TestClient(app)
        body = {"foo": "bar"}
        client.post(path, json=body)
        logs = _parse_logs(log_stream)
        actual = _extract_json_body(logs)

        assert actual is None, "Body logged"

    @pytest.mark.parametrize("path", ["error-async", "error-sync"])
    def test_limit_size_of_logged_request_body(self, app, log_stream, path):
        app.add(ns)
        client = TestClient(app)
        body = {"foo": 2 * CustomRoute.MAX_BODY_LENGTH * "xxx"}
        with pytest.raises(Exception):
            client.post(path, json=body)
        logs = _parse_logs(log_stream)
        assert len(logs) == 1
        [line] = logs
        m = re.match(r".*body: (?:b')?(.*)\)", line)
        assert m
        actual = m.group(1)
        assert len(actual) <= CustomRoute.MAX_BODY_LENGTH, "Body not logged"
