# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## Changed

- Allow overriding port by passing arguments to `run`, even when the environment
  variable `SERVER_PORT` is set.


## [v0.3.0] - 2021-11-18

## Added

- Log body at `ERROR` level on uncaught exceptions.
- Add function `run` to run the apish application with default settings.
- Add module `apish.main` to run a test application for easier debugging.
- Return HTTPExceptions and ValidationErrors as problem JSON.

## [v0.1.0] - 2020-08-26

### Added

- Initial release
