Description
============

Introduction
------------

Apian is an opinionated library for setting up a Python-based service with a
minimum of boilerplate. It is a thin wrapper around `FastAPI
<https://pypi.org/project/fastapi/>`_.
